/* eslint-disable no-unused-vars */
function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  console.log(ev.currentTarget);
  ev.dataTransfer.setData("text/html", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text/html");
  var newAudio = document.getElementById(data);
  newAudio.setAttribute("style", "margin-top:15rem; width: 260px;");
  ev.target.appendChild(newAudio);
}
