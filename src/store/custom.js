/* eslint-disable no-unused-vars */
import Vue from "vue";
/* eslint-enable no-unused-vars */

const state = {
  secCount: [],
  sec: 0,
  id: 0
};

const getters = {
  getSec(state) {
    return state.secCount;
  }
};

const mutations = {
  addSec(state) {
    let tempSec = [...state.secCount];
    let sec = {
      component: "panelcard.vue"
    };
    tempSec.push(sec);
    state.secCount = [...tempSec];
  }
};

export default {
  state,
  getters,
  mutations
};
