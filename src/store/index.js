import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import custom from "./custom.js";
/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-undef
const debug = process.env.NODE_ENV !== "production";
/* eslint-enable no-unused-vars */
export default new Vuex.Store({
  modules: {
    custom
  },
  strict: false
});

// export default new Vuex.Store({
//   state: {},
//   mutations: {},
//   actions: {},
//   modules: {}
// });
