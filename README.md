# Imadio

A simple UI where you can drag and drop your image and audio on each slide.

Link: https://imadio.netlify.com/

### Instructions

1. To add a new panel on left side -  hover on existing card and click on add icon.
2. Drag the audio tab only. (Dont resist to play it)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
